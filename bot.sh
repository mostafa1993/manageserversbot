#!/bin/bash

source colors.sh

get_status() {
  if [[ -z $bot_process_id ]]; then
    Red Bot is not running.
    read -p "Do you want to start it? [y/N]" user_answer
    [[ $user_answer == [yY] ]] && start
  else
    Yellow Bot is running with process id: $bot_process_id
    read -p "Do you want to restart it? [y/N]" user_answer
    [[ $user_answer == [yY] ]] && restart
  fi
}

start() {
  python3 $bot &
  [[ -z $1 ]] && Green Bot is stared seccessfully.
}

restart() {
  _stop "silent"
  start "silent"
  Green Bot is restared seccessfully.
}

_stop() {
  if [[ $bot_process_id =~ ^[0-9]+$ ]]; then
    kill $bot_process_id
    bot_process_id=$(ps aux | grep ${bot} | grep -v 'grep' | awk '{ print $2 }')
    [[ -n $bot_process_id ]] && kill -9 $bot_process_id
    [[ -z $1 ]] && Red Bot is stopped.
    return
  fi
  [[ -z $1 ]] && Red Bot was already stopped.
}

bot=server_manager.py
bot_process_id=$(ps aux | grep ${bot} | grep -v 'grep' | awk '{ print $2 }')
for arg in $@; do
  case $arg in
    status) get_status
      break;;
    start) start
      break;;
    restart) restart
      break;;
    stop) _stop
      break;;
    *) Red Error: invalid argument. Valid arguments: status, start, restart, stop && exit 1
      ;;
  esac
done
