red='\e[91m'
green='\e[92m'
yellow='\e[93m'
magenta='\e[95m'
cyan='\e[96m'
none='\e[0m'
Red() { echo -e ${red}$*${none}; }
Green() { echo -e ${green}$*${none}; }
Yellow() { echo -e ${yellow}$*${none}; }
Magenta() { echo -e ${magenta}$*${none}; }
Cyan() { echo -e ${cyan}$*${none}; }

