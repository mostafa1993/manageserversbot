#!/usr/bin/env python

import logging
import paramiko
import yaml
import time
import random
from time import sleep
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.constants import ParseMode
from telegram.ext import (
        Application,
        CommandHandler,
        ContextTypes,
        ConversationHandler,
        MessageHandler,
        filters,
        CallbackQueryHandler
        )


logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
logger = logging.getLogger(__name__)


ON, OFF = True, False
def lock(switch=ON):
  global user_chat_id
  if switch == ON:
    with open('.lock', 'w') as f:
      f.write(f'{user_chat_id}')
  else:
    with open('.lock', 'w') as f:
      pass
  return

def is_lock():
  global user_chat_id
  is_busy = False
  with open('.lock', 'r') as f:
    locked_id = f.read().strip()
  if locked_id:
    is_busy = locked_id != str(user_chat_id)
  return is_busy

def bold(text, is_md=False):
  if is_md:
    return f"*{text}*"
  return f"<b>{text}</b>"


def get_buttons():
  button1 = InlineKeyboardButton("Setup servers", callback_data=SETUP)
  button2 = InlineKeyboardButton("Reboot servers", callback_data=REBOOT)
  button3 = InlineKeyboardButton("Install VPN on servers", callback_data=INSTALL)
  button4 = InlineKeyboardButton("Uninstall VPN from servers", callback_data=UNINSTALL)
  button5 = InlineKeyboardButton("Thanks, I'm DONE.", callback_data=END)
  inline_keyboard = [[button1, button2], [button3, button4], [button5]]
  markup = InlineKeyboardMarkup(inline_keyboard)
  return markup


def get_servers():
  global servers_file
  try:
    with open(servers_file, 'r') as f:
      servers = yaml.load(f, yaml.SafeLoader)
    # Test the structure of file
    for server in servers:
      server['id'], server['ip'], server['username'], server['password']
    return servers
  except:
    return False

def gmt_time(is_md=False):
  h, m, s = list(time.gmtime())[3:-3]
  if is_md:
    return f"\[{h}:{m}:{s} GMT]"
  return f"[{h}:{m}:{s} GMT]"


async def start(update, context):
  global user_chat_id
  user_chat_id = update.message.chat_id
  if is_lock():
    await update.message.reply_text(f"Sorry, currently I'm busy with another user.\nTry again later by pressing /start.")
    return ConversationHandler.END
  lock()
  global menue_message_id
  menue_message_id = None
  chat_id = update.message.chat_id
  message_id = update.message.message_id
  await context.bot.delete_message(chat_id=chat_id, message_id=message_id)
  if menue_message_id:
    try:
      await context.bot.edit_message_text(chat_id=chat_id, message_id=menue_message_id,
              text=f"To manage your servers, first upload your target servers please.",
              parse_mode='Markdown')
      return ACTIONS
    except:
      pass
  sent = await update.message.reply_text(f"To manage your servers, first upload your target servers please.")
  menue_message_id = sent.message_id
  return ACTIONS


async def upload_database(update, context):
  global menue_message_id
  global servers_file
  chat_id = update.message.chat_id
  user_id = update.message.from_user.id
  message_id = update.message.message_id
  await context.bot.delete_message(chat_id=chat_id, message_id=message_id)
  file = (await update.message.effective_attachment.get_file())
  await file.download_to_drive(servers_file)
  servers = get_servers()
  if not servers:
    await context.bot.edit_message_text(chat_id=chat_id, message_id=menue_message_id,
            text=f"{bold('Target servers must be stored in a yaml file')}.\nTry to upload a correct file.",
            parse_mode='Markdown')
    return ACTIONS

  markup = get_buttons()
  sleep(1)
  clock = gmt_time(is_md=True)
  await context.bot.edit_message_text(chat_id=chat_id, message_id=menue_message_id,
          text=f"{bold('Target servers are uploaded successfully', is_md=True)} {clock}.\nWhat can I do?",
          reply_markup=markup, parse_mode='Markdown')
  return ACTIONS


async def setup(update, context):
  query = update.callback_query
  await query.answer()

  servers = get_servers()
  log = []
  for server in servers:
    ip = server['ip']
    port = server['port']
    username = server['username']
    password = server['password']
    root_password = server['root_password']
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip, port=port, username=username, password=password)
    tmp = [f"Server: {ip} is being setup.\n"]
    log += tmp
    text = ''.join(log)
    await query.edit_message_text(text=text, parse_mode='Markdown')
    stdin, stdout, stderr = ssh.exec_command(f'rm -rf {prepare_script} {v2ray_scrip}')
    stdin, stdout, stderr = ssh.exec_command(f'curl -s -L {prepare_script_url}')
    #stdin, stdout, stderr = ssh.exec_command("sudo bash prepare.sh", get_pty=True)
    #stdin.write(root_password+'\n')
    #stdin.flush()
    #print(stderr.read().decode())
    sleep(4)
    clock = gmt_time(is_md=True)
    tmp = [f"Server: {ip} is setup {clock}.\n"]
    del log[-1]
    log += tmp
    text = ''.join(log)
    await query.edit_message_text(text=text, parse_mode='Markdown')
    ssh.close()
    
  sleep(3)
  markup = get_buttons()
  text = ''.join(log)
  await query.edit_message_text(text=text+f"{bold('All servers are sutup', is_md=True)}.\nAny thing else?",
          reply_markup=markup, parse_mode='Markdown')
  
  return ACTIONS


async def reboot(update, context):
  query = update.callback_query
  await query.answer()

  servers = get_servers()
  text = ''
  for server in servers:
    ip = server['ip']
    port = server['port']
    username = server['username']
    password = server['password']
    root_password = server['root_password']
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip, port=port, username=username, password=password)
    stdin, stdout, stderr = ssh.exec_command(f'sudo reboot')
    stdin.write(root_password+'\n')
    stdin.flush()
    ssh.close()
    clock = gmt_time(is_md=True)
    sleep(3)
    text += f"Server: {ip} is rebooted {clock}.\n"
    await query.edit_message_text(text=text, parse_mode='Markdown')

  markup = get_buttons()
  await query.edit_message_text(text=text+f"{bold('All servers are rebooted', is_md=True)}.\nAny thing else?",
          reply_markup=markup, parse_mode='Markdown')
  return ACTIONS


async def install(update, context):
  query = update.callback_query
  await query.answer()
  servers = get_servers()
  log = []
  for server in servers:
    ip = server['ip']
    port = server['port']
    username = server['username']
    password = server['password']
    root_password = server['root_password']
    caddy_port = server['caddy_port']
    url_name = server['url_name']
    protocol = server['protocol']
    domain = server['domain']
    tmp = [f"VPN is being installed on server {ip}.\n"]
    log += tmp
    text = ''.join(log)
    await query.edit_message_text(text=text, parse_mode='Markdown')
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip, port=port, username=username, password=password)
    channel = ssh.get_transport().open_session()
    channel.get_pty()         # get a PTY
    channel.invoke_shell()    # start the shell before sending commands
    channel.send("sudo rm -rf /etc/v2ray")
    sleep(2)
    channel.send("git clone http://github.com/233boy/v2ray -b master /etc/v2ray/233boy/v2ray --depth=1"+ '\n')
    time.sleep(3)
    stdin, stdout, stderr = ssh.exec_command(f'rm -rf {prepare_script} {v2ray_scrip}')
    stdin, stdout, stderr = ssh.exec_command(f'curl -s -L {v2ray_scrip_url}')
    stdin, stdout, stderr = ssh.exec_command(f'curl -s -L {prepare_script_url}'); stderr.read().decode()
    sleep(2)
    stdin, stdout, stderr = ssh.exec_command(f'sudo bash {prepare_script} -d {domain} -k {protocol} -n {url_name} -P {caddy_port}')
    #print(stderr.read().decode())
    already_installed = stdout.read().decode().count('V2ray is already installed')
    already = 'already' if already_installed else ''
    stdin, stdout, stderr = ssh.exec_command(f'sudo bash {prepare_script} -L')
    url = stdout.read().decode()
    url = url[url.find('vmess'):]
    ssh.close()
    clock = gmt_time(is_md=True)
    tmp = [f"VPN is {already} installed on `{ip}` {clock}.\ncaddy-port: {caddy_port}\nCONFIG-URL: `{url}`\n\n"]
    del log[-1]
    log += tmp
    text = ''.join(log)
    await query.edit_message_text(text=text, parse_mode='Markdown')

  sleep(1)
  markup = get_buttons()
  clock = gmt_time(is_md=True)
  text = ''.join(log)
  await query.edit_message_text(text=text+f"{bold('VPNs are installed on all servers', is_md=True)} {clock}.\nAny thing else?",
          reply_markup=markup, parse_mode='Markdown')
  return ACTIONS


async def uninstall(update, context):
  query = update.callback_query
  await query.answer()
  servers = get_servers()
  log = []
  for server in servers:
    ip = server['ip']
    port = server['port']
    username = server['username']
    password = server['password']
    root_password = server['root_password']
    tmp = [f"VPN is being uninstalled from {ip}.\n"]
    log += tmp
    text = ''.join(log)
    await query.edit_message_text(text=text, parse_mode='Markdown')
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip, port=port, username=username, password=password)
    stdin, stdout, stderr = ssh.exec_command(f'rm -rf {prepare_script} {v2ray_scrip}')
    stdin, stdout, stderr = ssh.exec_command(f'curl -s -L {v2ray_scrip_url}')
    stdin, stdout, stderr = ssh.exec_command(f'curl -s -L {prepare_script_url}'); stdout.read().decode()
    sleep(2)
    stdin, stdout, stderr = ssh.exec_command(f'sudo bash {prepare_script} -U')
    print(stderr.read().decode())
    print("##############################################################")
    print(stdout.read().decode())
    sleep(2)
    ssh.close()
    clock = gmt_time(is_md=True)
    tmp = [f"VPN is uninstalled from `{ip}`\n\n"]
    del log[-1]
    log += tmp
    text = ''.join(log)
    await query.edit_message_text(text=text, parse_mode='Markdown')

  sleep(2)
  markup = get_buttons()
  clock = gmt_time(is_md=True)
  text = ''.join(log)
  await query.edit_message_text(text=text+f"{bold('VPNs are uninstalled from all servers', is_md=True)} {clock}. Any thing else?",
          reply_markup=markup, parse_mode='Markdown')
  return ACTIONS


async def end(update, context):
    old_text = update.callback_query.message.text
    old_text = old_text.replace('Any thing else?', '')
    old_text = old_text.replace('What can I do?', '')
    old_text = old_text.replace('[', '\[')
    i = 0
    while True:
      i = old_text.find('vmess://', i+13)
      if i == -1:
        break
      j = old_text.find('\n', i)
      url = old_text[i:j]
      old_text = old_text.replace(url, f"`{url}`")
    query = update.callback_query
    await query.answer()
    await query.edit_message_text(text=old_text+"\nSee you next time!\n"
            "You may get back to me by pressing: /start", parse_mode='Markdown')
    lock(OFF)
    user_chat_id = None
    return ConversationHandler.END


def main(token):
  lock(OFF)
  application = Application.builder().token(token).build()
  menue_handler = ConversationHandler(
          entry_points=[CommandHandler("start", start)],
          states={
              ACTIONS: [
                  MessageHandler(filters.ATTACHMENT, upload_database),
                  CallbackQueryHandler(setup, pattern="^" + str(SETUP) + "$"),
                  CallbackQueryHandler(reboot, pattern="^" + str(REBOOT) + "$"),
                  CallbackQueryHandler(install, pattern="^" + str(INSTALL) + "$"),
                  CallbackQueryHandler(uninstall, pattern="^" + str(UNINSTALL) + "$"),
                  CallbackQueryHandler(end, pattern="^" + str(END) + "$"),
                  ],
              },
          fallbacks=[CommandHandler("start", start)],
          )
  application.add_handler(menue_handler)
  application.run_polling()


# Global variables and CONSTANTS
menue_message_id, user_chat_id, servers_file = None, None, 'servers.yaml'
ACTIONS, SETUP, REBOOT, INSTALL, UNINSTALL, START_OVER, END, START = range(8)
prepare_script, v2ray_scrip = '/root/prepare.sh', '/root/v2ray.sh'
prepare_script_url = f'https://gitlab.com/mostafa1993/vps_preparation/-/raw/main/prepare.sh --output {prepare_script}'
v2ray_scrip_url = f'https://gitlab.com/mostafa1993/vps_preparation/-/raw/main/v2ray.sh --output {v2ray_scrip}'
if __name__ == '__main__':
  with open('bot.yaml', 'r') as f:
    token = yaml.load(f, yaml.SafeLoader)['token']
  main(token)

